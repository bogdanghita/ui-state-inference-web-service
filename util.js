function containsValue(array, value){
  var idx = array.indexOf(value);
  return idx > -1;
}

function deleteValue(array, value){
  var idx = array.indexOf(value);
  if(idx > -1){
    array.splice(idx, 1);
  }
}

exports.containsValue   = containsValue;
exports.deleteValue     = deleteValue;