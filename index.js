var _             = require('underscore');
var util          = require('./util.js');
var io            = require('socket.io')();
var generateId    = _.uniqueId;

var MOCK_INPUT_ID = 12345;
var MOCK_INPUT_INTERVAL_MS = 5;

var SEND_OUTPUT_INTERVAL_MS = 100;

var PORT = 9090;

var clients = {};
var outputListeners = [];

var appData = {};
var smData = {};


/* APP FLOW */

io.on('connection', function(socket){
  var id = generateId('client-');
  console.log("[connection] id=%s", id);
  clients[id] = socket;

  socket.on('disconnect', function(socket){
    console.log("[disconnect] id=%s", id);
    if(util.containsValue(outputListeners, id)){
      util.deleteValue(outputListeners, id);
    }
    delete clients[id];
  });

  socket.on('output:subscribe', function(){
    console.log("[output:subscribe] id=%s", id);
    if(!util.containsValue(outputListeners, id)){
      outputListeners.push(id);
    }
  });
  socket.on('output:unsubscribe', function(){
    console.log("[output:unsubscribe] id=%s", id);
    if(util.containsValue(outputListeners, id)){
      util.deleteValue(outputListeners, id);
    }
  });

  socket.on('input:sm', function(data){
    handleSmDataInput(id, JSON.parse(data));
  });
});

setInterval(sendSmData, SEND_OUTPUT_INTERVAL_MS);

io.listen(PORT);
console.log("Server listening on port %d", PORT);

// mockSmDataInput();


/* IMPLEMENTATION FUNCTIONS */

function handleSmDataInput(clientId, data){
  // console.log("[handleSmDataInput] id=%s, data=%s", clientId, JSON.stringify(data));
  var key = getKey(data.device_id, data.appInfo);

  if(!(key in smData)){
    smData[key] = [];
    appData[key] = {
      device_id: data.device_id,
      device_name: data.device_name
    };
  }
  appData[key].app_info = data.appInfo;
  smData[key] = smData[key].concat(data.data);
}

function sendSmData(){
  for(var key in smData){

    // send to all listeners 
    var len = outputListeners.length;   
    for(var i=0; i<len; i++){
      var socket = clients[outputListeners[i]];
      if(smData[key].length == 0){
        continue;
      }
      var data = {
        client_id: key,
        device_id: appData[key].device_id,
        device_name: appData[key].device_name,
        app_info: appData[key].app_info,
        values: smData[key]
      };
      socket.emit('output:sm', data);
    }
    // clear
    smData[key] = [];
  }
}

function getKey(device_id, app_info){
  return device_id + app_info.packageName;
}

/* TEST FUNCTIONS */

function generateMockData(size){
  if(!size){
    size = 10;
  }
  var array = [];
  for(var i=0; i<size; i++){
    var value = Math.floor(Math.random() * 10 + 1);
    var timestamp = new Date().getTime();
    array.push([timestamp, value]);
  }
  return {
    device_id: "a7sdf68h",
    device_name: "mock-dev",
    data: array
  };
}

function mockSmDataInput(){
  setInterval(function(){
    handleSmDataInput(MOCK_INPUT_ID, generateMockData(1));
  }, MOCK_INPUT_INTERVAL_MS);
}
